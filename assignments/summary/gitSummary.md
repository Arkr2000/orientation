# **Git Basics**
## What is Git?
Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency. The advantages of Git over other source control systems are:
 * Free and open source
 * Distributed
 * Data assurance
 * Staging area
 * Branching and merging
## Git Features
* ### Distributed System
  Distributed systems are those which allow the users to perform work on a project from all over the world. A distributed system holds a Central repository that can be accessed by many remote collaborators by using a Version Control System.
  ![img](https://media.geeksforgeeks.org/wp-content/uploads/20191203164948/Distributed-Version-Control-System.jpg)
* ### Compatibility
  Git is compatible with all the Operating Systems that are being used these days. Git repositories can also access the repositories of other Version Control Systems like SVN, CVK, etc.
* ### Non-linear Development
  Git allows users from all over the world to perform operations on a project remotely. A user can pick up any part of the project and do the required operation and then further update the project.
* ### Open-Source
  Git is a free and open-source distributed version control system designed to handle everything from small to very large projects with speed and efficiency. It is called open-source because it provides the flexibility to modify its source code according to the user’s needs. 

## Git Terminologies
* ### Repository
  Git repository is a directory that stores all the files, folders, and content needed for your project.  
* ### Remote 
  It is a shared repository that all team members use to exchange their changes.
* ### Master
  The primary branch of all repositories. All committed and accepted changes should be on the master branch.
* ### Branch
  A version of the repository that diverges from the main working project or master branch.
* ### Clone
  A clone is a copy of a repository or the action of copying a repository.
* ### Fetch
  By performing a Git fetch, you are downloading and copying that branch’s files to your workstation.
* ### Fork
  Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.  
* ### Merge
  Taking the changes from one branch and adding them into another (traditionally master) branch.
* ### Push
  Updates a remote branch with the commits made to the current branch. 
* ### Pull
  If someone has changed code on a separate branch of a project and wants it to be reviewed to add to the master branch, that someone can put in a pull request.

  ![git](https://www.earthdatascience.org/images/earth-analytics/git-version-control/git-fork-clone-flow.png)
  

## Git Workflow
* ### Working Directory
  The working area is like your scratch space, it's where you can add new content, you can modify delete content, if the content that you modify or delete is in your repository you don't have to worry about losing your work.The working area, in brief, is the files that are not in the staging area.
* ### Staging Area
  The staging area, that's where files are going to be a part of your next commit. It's how Git knows what is going to change between the current commit and the next one. 
* ### Repository
  The repository contains all of your commits. And the commit is a snapshot of what your working and staging area look like at the time of the commit. It's in your .git directory. 
![workflow](https://i.redd.it/nm1w0gnf2zh11.png)
## Basic Git Commands
![image](https://i0.wp.com/build5nines.com/wp-content/uploads/2020/04/Git-Cheat-Sheet-Build5Nines.jpg?fit=787%2C506&ssl=1)

